## 01 配置管理与项目部署概述

#### 1.配置管理CM

**配置管理( CM：Configuration Management ) 是一个系统工程过程，用于在产品的整个生命周期内建立和维护产品的性能、功能和物理属性与其要求、设计和操作信息的一致性。**

**1. 配置识别（Configuration Identification）**   id--唯一性编码：JAVA_HOME URL URI

**2. 配置控制（Configuration Control）** 变更 JDK8、JDK11、**JDK17** LTS(Java模块化、云原生)

**3. 配置状态统计（Configuration status accounting）** 次数、时间、作者...贡献率

**4. 配置审核（Configuration audits）**组员、组长审核。Calculator：+-*/%!^...

**在文档化的基础上，配置管理两个核心，就是需求管理和****变更管理****。**

**需求管理**，就是挖掘明示的和潜在的客户需求，分模块分层树状管理。成果就是基线（Baseline），产品必须和基线一致。

**变更管理**，就是在需要变更的时候怎么管好变更。变更的目的分两种，一种是纠正错误，一种是实施改进。孰能无过，但是如果需求管理做得好，变更执行到位，那错误就会少，企业那点钱，要么省下来变成工资和利润，要么投入到真正的改进。另外，少犯错误，大家心里也痛快点。

**20世纪60年代末70年代初,******一千四百万****次修改，Change and Configuration Control变更与配置控制CCC：最早的配置管理工具之一。

21世纪初提供**工作空间管理、并行开发支持、过程管理、权限控制、变更管理等**一系列全面的管理能力，已经形成了一个完整的理论体系。

**版本控制系统（version control system）：版本控制是一种在开发的过程中用于管理我们对文件、目录或工程等内容的修改历史，方便查看更改历史记录，备份以便恢复以前的版本的软件工程技术。**

**Git最热门的版本控制系统，也是本课程学习重点之一。**

**SCM三剑客：CVS最初主流、SVN（企业级）、Git（开源开放）**

工作空间(本机目录)

![image-20220912091433356](assets/image-20220912091433356.png)

克隆码云远程库cmpd_gitops

git clone https://gitee.com/osgisOne/cmpd_gitopsc.git

![image-20220912091805585](assets/image-20220912091805585.png)

git status

![image-20220912092331492](assets/image-20220912092331492.png)

添加文件git add .

![git push](assets/image-20220912092447894.png)

提交git commit -m "修改项目管理的内容" message

git push #本地仓库推送到码云远程仓库

![image-20220912092912028](assets/image-20220912092912028.png)

#### 2.项目部署PD

**软件部署（Software Deployment）是为将一个软件系统投入使用而进行的所有活动，包括硬件配置、软件的安装、环境变量设置等。在一些机器上批量安装某一程序也称为软件部署，分为指派与发布两种类型。**

**蓝绿部署是指在部署过程中同时运行两个版本的程序**。新旧版本。

**滚动发布是指在升级过程中，逐台逐台的替换旧版本服务器**。

**灰度发布也叫金丝雀发布**。

**持续集成 CI**（Continuous Integration）：是一种软件项目管理方法，依据资产库（源码，类库等）的变更自动完成编译、测试、部署和反馈。

![preview](assets/c5c8e6f40c7c133e22402c00bb7e1a25_r.jpg)

**持续交付CD**（Continuous Delivery）：持续交付在持续集成的基础上，**【手工】**将集成后的代码部署到更贴近真实运行环境的「**类生产环境**」（*production-like environments*）中。

开发环境--配置高性能高   测试环境--配置最低   （类）生产环境--平均

![preview](assets/db7198e3c39e4656e18efcb4bd1b20b1_r.jpg)

持续部署CD（Continuous Deployment）：持续部署则是在持续交付的基础上，把**部署到生产环境的过程自动化**。生产环境就是用户环境。

![img](assets/f96f19e4d567aad5006d841963a86e41_b.png)

**开发运维DevOps**

![img](assets/devops.jpeg)

#### 小结

配置管理：一致性；需求管理、变更管理；VCS；CVS、SVN、Git

项目部署：指派和发布；CI/CD；DveOps

Git Bash(5个指令)：

| 序号 | 名称   | 用法                | 描述                   | 示例                                                      |
| ---- | ------ | ------------------- | ---------------------- | --------------------------------------------------------- |
| 1    | clone  | git clone URL       | 克隆，远程库克隆本地库 | git clone URL https://gitee.com/osgisOne/cmpd_gitopsc.git |
| 2    | status | git status          | 库状态                 | git status                                                |
| 3    | add    | git add .           | 添加当前目录所有文件   | git add .                                                 |
| 4    | commit | git commit -m "***" | 提交暂存区文件入库     | git commit -m "描述提交内容的信息"                        |
| 5    | push   | git push            | 推送本地库至远程库     | git push                                                  |