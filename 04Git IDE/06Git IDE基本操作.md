##  Git IDE基本操作

#### **0.便携式VS Code及其扩展安装**

https://code.visualstudio.com/docs/editor/portable

![image-20221110225344225](assets/image-20221110225344225.png)

Code.exe同目录新建data，data目录下保存安装的扩展，并新建tmp目录。初始界面。

![image-20221110225646100](assets/image-20221110225646100.png)

VS Code扩展安装：中文，输入chinese，选择安装。Java，安装java扩展

![image-20221110230648302](assets/image-20221110230648302.png)

![image-20221111104451605](assets/image-20221111104451605.png)

![image-20221111104634471](assets/image-20221111104634471.png)

![image-20221111104700839](assets/image-20221111104700839.png)

![image-20221111104748602](assets/image-20221111104748602.png)

![image-20221111111103360](assets/image-20221111111103360.png)

VS Code其它扩展Git，Gitee，Jenkins等参考帮助文档。

![image-20221111111751805](assets/image-20221111111751805.png)

VS Code资源视图

![image-20221111113640897](assets/image-20221111113640897.png)

VS CodeGit视图-Mod取模或求余运算

![image-20221111113711144](assets/image-20221111113711144.png)

Commit提交，输入message后，CTRL+ENTER快捷键进行提交，或者点击Commit按钮。

![image-20221111113809234](assets/image-20221111113809234.png)

![image-20221111114014364](assets/image-20221111114014364.png)

VS Code将git的add和commit两个指令合并。

![image-20221111114126441](assets/image-20221111114126441.png)

![image-20221111114306984](assets/image-20221111114306984.png)

![image-20221111113936265](assets/image-20221111113936265.png)

![image-20221111114641161](assets/image-20221111114641161.png)

![image-20221111114720718](assets/image-20221111114720718.png)



#### **1.VS Code配置Git**

VS Code内置Git支持/功能(仍需安装Git的，且**Git版本V2+**)
		VS Code配置Git

![img](assets/9d212d99c34f91aa073d9db08e2184b4.png)

VS Code配置为Git编辑器

![img](assets/7630d5155aeae4be37443bf86dcdb473.png)

#### 2.VS Code安装各种Git扩展包

![img](assets/5a6707a56499b7cf2210af036372d801.png)

- [Git History (git log)](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory) - View git log, file or line history.
- [Project Manager](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager) - Easily switch between projects.
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) - GitLens **supercharges** the built-in Visual Studio Code Git capabilities. It helps you to **visualize code authorship** at a glance via inline Git blame annotations and code lens, **seamlessly navigate and explore** the history of a file or branch, **gain valuable insights** via powerful comparision commands, and so much more.
- [gitignore](https://marketplace.visualstudio.com/items?itemName=codezombiech.gitignore) - Language support for .gitignore files. Lets you pull .gitignore files from the https://github.com/github/gitignore repository.
- [Open in GitHub / Bitbucket / VisualStudio.com](https://marketplace.visualstudio.com/items?itemName=ziyasal.vscode-open-in-github) - Jump to a source code line in Github / Bitbucket / VisualStudio.com.

![img](assets/e0050a36f733fab2db8d95bc7525c628.png)

![img](assets/650df4a01590be679247733be9c05423.png)

#### 3.VS Code新建Git仓库

![img](assets/ebb28f393c49e09d76b912f8d6d01401.png)

#### 4.VS Code日常用源代码管理

各种功能菜单，如文档差异。

![img](assets/7d59b4899c8cf3a9cb5121db325c4345.png)

![img](assets/035f7aa994531deeda74e9c4c53135c9.png)

![img](assets/5dd15a357e36ae7e49a2725b8d0e21bf.png)

![img](assets/5e4803be07ed5bc97b783de3b8f073b3.png)

![image-20221111114701213](assets/image-20221111114701213.png)

#### 5.VS Code分支合并可选不同参数合并

![img](assets/0e68057a6aa4884e7b54e3a95253aff6.png)

#### 6.VS Code漂亮的图示分支

![img](assets/c8c1a04a76cd1d61be7456b64af3391c.png)

#### 7.VS Code配置GitHub：open in GitHub

![img](assets/6dfa5b4b73a57bfb48b0624c37cf063b.png)

#### 8.VS Code官方文档

Code中使用版本控制。关于Git：https://code.visualstudio.com/Docs/editor/versioncontrol

![img](assets/08fd07769eb18d3714a073d2925b2904.png)

Code中GitHub。关于GitHub：

![img](assets/79bd96c3e198e46272dfdfc891bc3c52.png)

![img](assets/3761320d2a68ef33d6a221efe907fb41.png)

![img](assets/66f412540f522706fde5b620794122ba.png)

![img](assets/82aebb3d6c65e0f2240228965d24556e.png)

![img](assets/4515ee1794df26b46a8216a028d637bd.png)

![img](assets/50116801343628a0164e66116502ffa9.png)

Github存储库扩展。关于GitHub REPO: VSCode与GitHub远程库直接交互操作。

![img](assets/4ceceaee1da5ec7b857ef1f9c844f5a3.png)

![img](assets/d62c3a771f12c5842a6a8e7ff8e44928.png)

![img](assets/66a7b45c8841eecd56cfd39ea3b3d557.png)

![img](assets/533a5aa38ff75ed1a43e723ad6200e8c.png)

![img](assets/b4734ee165174d83a3bddb30e975bc2a.png)

VS Code 受限模式。

![img](assets/70d2571d96c8b29340bbf55d4733c977.png)

#### 9.其它Git IDE

https://git.wiki.kernel.org/index.php/InterfacesFrontendsAndTools



![image-20221104111630771](assets/image-20221104111630771.png)

#### 小结

Git IDE增强Git操作可视化，提高开发效率（？）。

| 序号 | 名称          | 用法                     | 描述                         | 示例                                                         |
| ---- | ------------- | ------------------------ | ---------------------------- | ------------------------------------------------------------ |
| 1    | clone         | git clone URL            | 克隆，远程库克隆本地库       | git clone URL https://gitee.com/osgisOne/cmpd_gitopsc.git    |
| 2    | status        | git status               | 库状态                       | git status                                                   |
| 3    | add           | git add .                | 添加当前目录所有文件         | git add .                                                    |
| 4    | commit        | git commit -m "***"      | 提交暂存区文件入库           | git commit -m "描述提交内容的信息"                           |
| 5    | push          | git push                 | 推送本地库至远程库           | git push                                                     |
| 6    | checkout      | git checkout develop     | 签出分支                     | git checkout develop                                         |
| 7    | merge         | git merge hotfix         | 快进式合并hotfix到当前分支   | git merge hotfix，将hotfix直接合并到当前分支，当前分支线性化，掩盖历史 |
| 8    | merge         | git merge --no-ff hotfix | 非快进式合并hotfix到当前分支 | git merge hotfix，将hotfix创建新结点，再合并到当前分支，保留hotfix分支，当前非线性化，保留历史 |
| 9    | branch        | git branch -a -b name    | 分支                         | 显示、创建、切换分支                                         |
| 10   | rebase        | git rebase master        | 变基                         | 将当前分支变基到master分支                                   |
| 11   | log           | git log                  | 日志                         | git log --oneline --decorate --graph --all                               git log --graph --all --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative |
| 12   | config        | git config               | 配置                         | git config --global user.name "ldl"                                                                 git config --global user.email "ldl@chzu.edu.cn" |
| 13   | reset         | git reset HEAD^^         | 复位                         | git reset [--soft \| --mixed \| --hard] [HEAD]   1,2..^或数字     回退版本(^或数字指定) |
| 14   | prune         | git prune                | 剪支                         | 剪支，从对象数据库中删除所有不可访问的对象。在大多数情况下，用户应该运行 git gc，它调用 git prune |
| 15   | 汉化GitGUI    | zh_cn.msg                | 下载                         | 下载zh_cn.msg文件(https://github.com/stayor/git-gui-zh)     /mingw64/share/git-gui/lib/msgs/ |
| 16   | 定制GitGUI    | 编辑-->选项              |                              | 定制Git当前库或全局（所有版本库）：用户、邮箱、工具、编码等  |
| 17   | GitIDE VSCode | VSCode便携式IDE          | 下载                         | Code.exe目录新建data及其下的tmp文件夹                        |
| 18   | GitIDE VSCode | Exentions Pack for Java  | 安装                         | https://code.visualstudio.com/docs/java/java-tutorial [Language Support for Java™ by Red Hat](https://marketplace.visualstudio.com/items?itemName=redhat.java) [Debugger for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-debug) [Test Runner for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-test) [Maven for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-maven) [Project Manager for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-dependency) [Visual Studio IntelliCode](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode) |
| 19   | GitIDE VSCode | Git Extension Pack       | 安装                         | https://marketplace.visualstudio.com/items?itemName=donjayamanne.git-extension-pack                                                [Git History (git log)](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory)     [Project Manager](https://marketplace.visualstudio.com/items?itemName=alefragnani.project-manager)    [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)  [gitignore](https://marketplace.visualstudio.com/items?itemName=codezombiech.gitignore)        [Open in GitHub / Bitbucket / VisualStudio.com](https://marketplace.visualstudio.com/items?itemName=ziyasal.vscode-open-in-github) |
