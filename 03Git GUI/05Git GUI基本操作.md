##  Git GUI基本操作

#### **1.配置与汉化**

下载zh_cn.msg文件(https://github.com/stayor/git-gui-zh)，放置/mingw64/share/git-gui/lib/msgs/ 目录，如没有msgs文件夹，则新建msgs，再放入zh_cn.msg文件即可汉化Git GUI。

![img](assets/b5320dbda10f1688ec366576f1c5f886.png)

SSH配置

![img](assets/910354e6b4981a2e8d3044c08e5bcfb2.png)

#### 2.建库（本地库）

空白工作区时，显示下面对话框。

![img](assets/21c8621d4fb2f6113e811a55713a8517.png)

![img](assets/b26a46202a09833524aa3e70430d5262.png)

如打开已有库，可以右键：Git GUI Here或者从Git Bash终端执行Git GUI切换。

#### 3.建支

![img](assets/09123abd4ac7658acd7d12ae77fe8260.png)

#### 4.开发项目

Calculator实验项目，分多次完成研发任务。可以里程碑、基线、版本、标签等进行规划。

![img](assets/391ecc104fb66c33fa228e3c055e77aa.png)

  显然，Calculator项目的命名没有GuestList命名规范。

**重新扫描、缓存改动（add）、签名、提交(commit)**多次重复操作。

签名：-s, --signoff     add a Signed-off-by trailer

![image-20221106202851244](assets/image-20221106202851244.png)

#### 5.提交

![img](assets/ce4114445b3b5062792a11d3a83d57c5.png)

**上传**是git push推送本地库到远程库。

#### 6.合并

![img](assets/bfe73a4e39941bd5b2d2b9cd2e4b7cee.png)

注意：如合并错了可以在合并后的分支上，复位Reset即可恢复。

#### 7.添加远程库

![img](assets/9ccfb9b8c767a75cc870c269e5653bbf.png)

![img](assets/9970fee5a9a6c73269fc8eb4f9ed3453.png)

#### 8.上传远程库

![img](assets/5d39908c06520a47ae985f9dc6dad273.png)

**注意：**

**$ git push=fetch+merge**

**$ git gc = prune**

#### 9.图示所有分支历史

![img](assets/03f1930031a000da8868d0f55da0f44f.png)

#### 10.其它Git GUI客户端

![img](assets/335898a9c23c39017f35ae006e80d993.png)

#### 小结

Git GUI增强Git操作可视化，提高开发效率（？）。

| 序号 | 名称       | 用法                     | 描述                         | 示例                                                         |
| ---- | ---------- | ------------------------ | ---------------------------- | ------------------------------------------------------------ |
| 1    | clone      | git clone URL            | 克隆，远程库克隆本地库       | git clone URL https://gitee.com/osgisOne/cmpd_gitopsc.git    |
| 2    | status     | git status               | 库状态                       | git status                                                   |
| 3    | add        | git add .                | 添加当前目录所有文件         | git add .                                                    |
| 4    | commit     | git commit -m "***"      | 提交暂存区文件入库           | git commit -m "描述提交内容的信息"                           |
| 5    | push       | git push                 | 推送本地库至远程库           | git push                                                     |
| 6    | checkout   | git checkout develop     | 签出分支                     | git checkout develop                                         |
| 7    | merge      | git merge hotfix         | 快进式合并hotfix到当前分支   | git merge hotfix，将hotfix直接合并到当前分支，当前分支线性化，掩盖历史 |
| 8    | merge      | git merge --no-ff hotfix | 非快进式合并hotfix到当前分支 | git merge hotfix，将hotfix创建新结点，再合并到当前分支，保留hotfix分支，当前非线性化，保留历史 |
| 9    | branch     | git branch -a -b name    | 分支                         | 显示、创建、切换分支                                         |
| 10   | rebase     | git rebase master        | 变基                         | 将当前分支变基到master分支                                   |
| 11   | log        | git log                  | 日志                         | git log --oneline --decorate --graph --all                               git log --graph --all --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative |
| 12   | config     | git config               | 配置                         | git config --global user.name "ldl"                                                                 git config --global user.email "ldl@chzu.edu.cn" |
| 13   | reset      | git reset HEAD^^         | 复位                         | git reset [--soft \| --mixed \| --hard] [HEAD]   1,2..^或数字     回退版本(^或数字指定) |
| 14   | prune      | git prune                | 剪支                         | 剪支，从对象数据库中删除所有不可访问的对象。在大多数情况下，用户应该运行 git gc，它调用 git prune |
| 15   | 汉化GitGUI | zh_cn.msg                | 下载                         | 下载zh_cn.msg文件(https://github.com/stayor/git-gui-zh)     /mingw64/share/git-gui/lib/msgs/ |
| 16   | 定制GitGUI | 编辑-->选项              |                              | 定制Git当前库或全局（所有版本库）：用户、邮箱、工具、编码等  |
