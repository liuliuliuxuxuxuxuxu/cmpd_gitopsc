#### Jenkins安装配置



##### 0、安装Jenkins

Jenkins是开源CI&CD软件领导者， 提供超过1000个插件来支持构建、部署、自动化， 满足任何项目的需要。核心是Pipeline和Blue Ocean。

![image-20221127205430264](assets/image-20221127205430264.png)

Java 8—无论是Java运行时环境（JRE）还是Java开发工具包（JDK）都可以。

Jenkins通常作为一个独立的应用程序在其自己的流程中运行， 内置[Java servlet](https://stackoverflow.com/questions/7213541/what-is-java-servlet)容器/应用程序服务器（[Jetty](http://www.eclipse.org/jetty/)）。Jenkins也可以运行在不同的Java servlet容器(（如[Apache Tomcat](http://tomcat.apache.org/) 或 [GlassFish](https://javaee.github.io/glassfish/)）)中作为servlet运行。

java -jar jenkins.war --httpPort=9090(默认8080)

**解锁 Jenkins。自定义jenkins插件。创建第一个管理员用户。**

1、安装插件

如git，gitee，gitea，Maven，JDK，SonarQube，Pipeline，Blue Ocean等安装配置。

2、全局配置(global)

git、mvn、JDK、Tomcat等配置。

3、系统配置

主目录(.jenkins)、jenkins location、邮件、邮件通知等系统级配置。

4、管理用户

新建用户（个人和团队成员）。

5、任务操作

每个Item基本包括：General-源码管理-构建服务器-构建-构建执行的shell-构建后活动等。构建脚本工具有Make/Shell、Ant、Maven、Gradle等。

5.1、持续集成CI

高频率出现，主要是持续集成。交付和部署都为手工操作，测试和部署为持续集成服务器完成，没有专门的测试和部署。

5.2、持续交付/部署CD

项目打版本或建里程碑。持续交付和持续部署都为Jenkins自动完成操作，测试和部署有或没有专门的测试和部署环境。

5.3、建立流水线

建立流水线Pipeline。以Blue Ocean监控项目。

Jenkins的流水线?
Jenkins 流水线 (或简单的带有大写"P"的"Pipeline") 是一套插件，它支持实现和集成 continuous delivery pipelines 到Jenkins。

对Jenkins 流水线的定义被写在一个文本文件中 (成为 [`Jenkinsfile`](https://www.jenkins.io/zh/doc/book/pipeline/jenkinsfile))，该文件可以被提交到项目的源代码的控制仓库。这是"流水线即代码"的基础; 将CD 流水线作为应用程序的一部分，像其他代码一样进行版本化和审查。 创建 `Jenkinsfile`并提交它到源代码控制中提供了一些即时的好处:

- 自动地为所有分支创建流水线构建过程并拉取请求。
- 在流水线上代码复查/迭代 (以及剩余的源代码)。
- 对流水线进行审计跟踪。
- 该流水线的真正的源代码, 可以被项目的多个成员查看和编辑。