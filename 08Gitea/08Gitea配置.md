### Gitea配置与应用

##### 1.自托管Git服务

Gitea：一个无痛的自托管Git服务。其与Gitee, GitHub, Bitbucket or Gitlab等比较类似。

![img](assets/7e9fe0a091eff48a60c41109bf18be6a.png)

##### 2. Gitea配置

零配置应用。双击exe开始使用。

![image-20221122172344326](assets/image-20221122172344326.png)

Gitea运行期间不要关闭此窗口，否则Gitea停止服务。

##### 3.自定义 Gitea 配置 

Gitea 引用 custom 目录中的自定义配置文件来覆盖配置、模板等默认配置。

![f738d8dc59bd42e38dbfa4c00a334f45.png](assets/f738d8dc59bd42e38dbfa4c00a334f45.png)

双击Gitea应用，初始配置后得到配置文件：custom\conf\app.ini

![image-20221122163540507](assets/image-20221122163540507.png)APP_NAME = 21ST-GitOps-Gitea-GitServer  --Gitea服务器名称
RUN_USER = dll--OS用户或者改为git

[database]--默认
DB_TYPE  = sqlite3
PATH     = ./data/gitea.db

[repository]
ROOT = ./data/gitea-repositories

[server]
SSH_DOMAIN       = localhost
DOMAIN           = localhost
HTTP_PORT        = 3000
ROOT_URL         = http://localhost:3000/
START_SSH_SERVER = true

[log]
ROOT_PATH = ./log

[lfs]
PATH = ./data/lfs

##### 4.SSH认证加密访问Gitea

**ssh-keygen -t rsa -C ldl@chzu.edu.cn**

![img](assets/64a145707ce016c0267bbc73ea3f6673.png)

在 gitea ->settings -> SSH and GPG keys页面中，新创建SSH key，粘贴 公钥 id_rsa.pub 内容到对应文本框中

Gitea配置SSH

START_SSH_SERVER = true

![img](assets/251522d9859c4789df72b73fb2dcb441.png)

C:\Users\dll\.ssh\id_rsa.pub全部内容拷贝、粘贴到Gitea的SSH密钥

![img](assets/8c2fe9a643808d37f7d12ce7ff152c0f.png)

重启Gitea测试Gitea各项功能

##### 5.配置Web钩子

当Gitea事件发生时，Web钩子自动发出HTTP POST请求。![img](assets/543b720ce8be2ee85fb979909e47a0c6.png)

##### 6.Gitea其它配置

*配置数据库，默认为SQLite3。*

*注册为Windows服务。*

*部署Gitea公网。*

##### 7.Gitea应用

支持活动时间线

支持 SSH 以及 HTTP/HTTPS 协议

支持 SMTP、LDAP 和反向代理的用户认证

支持反向代理子路径

支持用户、组织和仓库管理系统

支持添加和删除仓库协作者

支持仓库和组织级别 Web 钩子（包括 Slack 集成）

支持仓库 Git 钩子和部署密钥

支持仓库工单（Issue）、合并请求（Pull Request）以及 Wiki

支持迁移和镜像仓库以及它的 Wiki

支持在线编辑仓库文件和 Wiki

支持自定义源的 Gravatar 和 Federated Avatar

支持邮件服务

支持后台管理面板

支持 MySQL、PostgreSQL、SQLite3、MSSQL 和 TiDB(MySQL) 数据库

支持多语言本地化（21 种语言）

支持软件包注册中心

![3ab7cd5f580086946fbebb88bd552d6b.png](assets/3ab7cd5f580086946fbebb88bd552d6b.png)

##### 8.新建用户

![6f04a2785ee87b3a8f4f78446e40123b.png](assets/6f04a2785ee87b3a8f4f78446e40123b.png)

新建仓库/项目

仓库与项目对应。

![2c42b66bc3bcfb00cb64de5db632161e.png](assets/2c42b66bc3bcfb00cb64de5db632161e.png)

##### 9.添加协作者

再添加一个用户，并设置为仓库的协作者。一人两个账号，模拟小组协作。

##### 10.配置钩子

当Git推送或PR时，触发Jenkins进行自动构建（见前面）。

##### 11.工单管理

团队协作时，通过工单进行协作。

##### 12.合并请求

各仓库成员都是在feature分支进行开发，合并到develop分支，再合并到master分支。

![image-20221124213220818](assets/image-20221124213220818.png)

![image-20221124213424708](assets/image-20221124213424708.png)

![image-20221124213725533](assets/image-20221124213725533.png)

![image-20221124214717601](assets/image-20221124214717601.png)

##### 13.建里程碑

每个feature开发、每次合并都可以建立标签、里程碑。

![image-20221124211425430](assets/image-20221124211425430.png)

##### 14.跨库协作

将Gitea仓库克隆到码云，设置镜像仓库URL：https://gitee.com/osgisOne/gitea-jwscalculatorg.git

![image-20221124220427341](assets/image-20221124220427341.png)

从Gitea克隆Fork仓库到Gitee平台进行跨库协作。Fork会保留它的来源，并可再合并回原来的仓库。

![image-20221124220359053](assets/image-20221124220359053.png)



##### 15.小结

无痛的自托管Git服务。

零配置应用。
