# cmpd_gitops

21软件技术专业1、2班《配置管理与项目部署》课程仓库，教学笔记与仓库操作和部署演示。

## 1步：创建笔记库

![img](assets/8a7fef4266a0e5b5b7d367effe1dd9f8.png)

## 2步：Feature分支创建文件夹

![img](assets/3d4c7cbaa397746762c39d0471ba1229.png)

![img](assets/36629d39e54c5cc27f416bb5fe8f5b5c.png)

## 3步：笔记库地址

https://gitee.com/osgisOne/cmpd_gitopsc.git

## 4步：克隆笔记库（全克隆）

![img](assets/7832d7195d177b9a19e82fd1395a652c.png)

![img](assets/7b56d204af79ed211527b248cec5e66f.png)

## 5步：克隆笔记库feature分支（方法1）

git clone -b feature --single-branch https://gitee.com/osgisOne/cmpd_gitopsc.git

![img](assets/4dd13c9dc21257ed7d63cb819393fea8.png)

删除本地库，再克隆

![img](assets/ad6cf6e4d068ac46d462569a759a0089.png)

切换目录到cmpd_gitopsc

![img](assets/b5e221ac6e3f723370945bb4961bcfac.png)

![img](assets/14515d9ef139a4648eb74a299978ad45.png)

## 6步：本地库创建并切换feature支（方法2）

先全克隆远程库，再映射远程库

git checkout -b feature origin/feature

![img](assets/2250a5211f74854920bd5c6c624d9dd8.png)

![img](assets/1f343ae526e859b83558a478fd11d401.png)

## 7步：Typroa编辑README.md

![img](assets/919f40c3dfa21c611f17f2808275d06b.png)

## 8步：编辑前后的状态

![image-20220827162157938](assets/image-20220827162157938.png)

git status

## 9步：添加提交本地库

git add .

git commit -m "修改Readme文件"

## 10步：推送至远程库

git push

(输入Gitee的账号、密码)

![image-20220827162910904](assets/image-20220827162910904.png)

## 11步：远程库查看

![image-20220827164116168](assets/image-20220827164116168.png)

## 12步：重复7-11步

![image-20220827163506237](assets/image-20220827163506237.png)

## 13步：第二次访问Pull

![image-20220827170652893](assets/image-20220827170652893.png)

新增“jwscalculator项目”文件夹，所以，总是pull开始每一天的工作。

## 14步：上传jwscalculator项目源码

## ![image-20220827171050673](assets/image-20220827171050673.png)

复制源码到本地库的“cmpd_gitopsc\jwscalculator项目”目录
**注意：删除原来项目文件夹下的各版本中的.git库**

![image-20220827181701099](assets/image-20220827181701099.png)

## 15步：编辑本文档并重复9-11步

![image-20220827173525231](assets/image-20220827173525231.png)

![image-20220827173556105](assets/image-20220827173556105.png)

![image-20220827173621991](assets/image-20220827173621991.png)

![image-20220827173703955](assets/image-20220827173703955.png)

## 16步：学习笔记撰写流程完毕

![image-20220827174056604](assets/image-20220827174056604.png)

![image-20220827174124332](assets/image-20220827174124332.png)

## 小结

创建Gitee远程库，选择Feature或Develop分支，克隆全库或单分支到本地库，推荐md格式，使用Typora工具，修改学习笔记，增加提交本地库，再推送远程库。后续，先拉后推，期间重复着：编程学习笔记，提交推送。
