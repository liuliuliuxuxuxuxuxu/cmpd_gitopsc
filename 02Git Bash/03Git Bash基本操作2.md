## Git Bash基本操作2

#### 1.Git分支特点

提倡多分支管理。

创建项目时，会针对不同环境（Build、Test、Staging、Product）创建三个常设分支：develop、pre-release、master。

临时或支承分支帮助解决团队成员之间的平行开发、发行准备及现场部署所带来的问题。这些分支的生命周期是有限的，最终肯定会被移除。如feature、fixbug/hotfix。

#### 2.Git各分支（常见分支）

###### master 分支 

将 origin/master 作为一条主分支（git默认分支，master涉嫌西方种族歧视，不再使用，github称为main），其头指针 HEAD 总是指向一个生产就绪的状态。所以，每一次有修改部分被合并到 master 分支时，就意味着一个新的产品已经被发行出来，即git脚本将会自动构建或回滚软件，并将其部署到生产服务器中。每次发布打上Tag,如: 0.1, 0.2或0.3。

###### develop 分支

把 origin/develop 作为另一条主分支，其头指针 HEAD 总是指向最新一次提交的开发更新版本，该更新版本用于下一个版本的发行。这也就是说，当 develop 分支上的代码达到了一个相对稳定的、能发行的时候，所有在 develop 分支上的代码都应该被合并到 master 分支上去，并用一个新的发行号来标记它。

###### Feature 分支

从 develop 分支分离出来，最终会合并到 develop 分支中去。某些情况下,新功能可能需要在线上的版本保持一致，可以从Master里拉出功能分支。

有时，我们开发新功能的时候，并不知道该新功能将会在未来的哪个版本发行，甚至会被丢弃。这时就可以使用 Feature 分支，当该新功能开发完成时，Feature 分支会被合并到 develop 分支，或者直接被丢弃。

###### Release 分支

Release 分支从 develop 分支分离，最终合并到 master/develop 分支中去。

Release 分支用于为新产品的发行作准备，如漏洞修补，准备元数据等。若当前版本为1.1.5的产品有一个大的版本1.2（而不是1.1.6或者2.0）即将推出，，那么我们就会从 develop 分支分离一条 Release 分支，当该 Release 分支到达能真正发行的状态时，就可以将其合并到 master 分支上，同时，为保证以后的发行不会遗漏这些小漏洞的修复，还需将其合并回 develop 分支。

注意，严格禁止在 release 分支上添加新的大功能。

###### Hotfix 分支

Hotfix 分支从 master 分支分离，最终合并到 master/develop 分支中去。

###### 远程分支

远程引用是对远程仓库的引用（指针），包括分支、标签等。 

###### 跟踪分支

从一个远程跟踪分支检出一个本地分支会自动创建所谓的“跟踪分支”（它跟踪的分支叫做“上游分支”）。

###### 分支命名规范

**| 分支类型 | 命名 | 说明 |**

| 主干 | master |

| 开发分支 | develop |

| 功能分支 | feature-* | 使用需求编号(如对应的需求编号过多,也可用拼音缩写) |

| 预发布分支 | release-* | 使用日期+版本号 , 如: 20201116-1.0 |

git version 2.33.0.windows.2

| bug分支 | fixbug-* hotfix-* | 使用bug工单号,或者根据当前tag版本号增加 |

| 产品分支 | production-* | 产品分支,对于独立进化的分支,如本地化部署 |

#### 3.Git分支模型（Git、Gitee等）

###### Git分支模型

![image-20220927100546995](assets/image-20220927100546995.png)

###### Gitee分支模型

![image-20220926105013626](assets/image-20220926105013626.png)

#### 4.Git合并（自动和人工合并及配置合并工具）

**默认情况下，Git执行"快进式合并"（fast-forward merge）**，会直接将myfeature分支指向Develop分支。使用--no-ff参数后，会执行正常合并，在Master分支上生成一个新节点。

git merge [--no-ff] 被合并的分组   #合并到当前分支

![webp](assets/webp.webp)

自动合并（没有冲突情况）。

#### 5.Git冲突（重难点）

**同文同行不同内容，发生冲突**

人工合并

工作区打开冲突文件，人工修改后，保存、添加、提交。

![4b36518b821628ab122339b3bf4820ac.png](assets/4b36518b821628ab122339b3bf4820ac.png)

![f0efbadc3820bf0b9b093e5762c04ba6.png](assets/f0efbadc3820bf0b9b093e5762c04ba6.png)

Git Bash终端，git mergetool，打开Git配置的合并工具，合并后，保存、添加、提交。此时在冲突的文件同名目录下多出来一个同名的.orig 文件。保留冲突文件的原始内容是git的默认逻辑，如果希望不要保存，可以使用以下配置来避免git就不会再自动产生 .orig 文件。

git config --global mergetool.keepBackup false

#### 6.Git配置字符集与合并工具

Git Bash右键，选项，文本，字符集：GBK。

![image-20220927095137914](assets/image-20220927095137914.png)

Java工具之一：记事本。另存为，字符集ANSI，**UTF-8不行**！

![img](assets/13b60ce5b1a954cab982d8886c7f3bd6.png)

Java工具之二：VS Code。设置，文本编辑器，Encoding:GBK

![image-20220927094910321](assets/image-20220927094910321.png)

Java工具之三：vim。此时不用设置，但需会用vim。

vim jwscalculator/src/main/java/gitops/jwscalculator/JwsCalculator.java

![image-20220927095700417](assets/image-20220927095700417.png)

**Git默认差异与合并工具是vim**

设置差异与合并工具指令：***mymerge***可以是vim code notepad等编辑器。

git config merge.tool ***mymerge***

git config merge.***mymerge***.cmd '***mymerge***.exe --base "$BASE" "$LOCAL" "$REMOTE" -o "$MERGED"'

git bash窗口： git config --list

显示差异与合并工具 code是指vs code

mergetool.default-mergetool.cmd=code --wait --diff $LOCAL $REMOTE

difftool.default-difftool.cmd=code --wait --diff $LOCAL $REMOTE

![img](assets/210f010246c3f7c2bbcc322edf89e46f.png)

#### 小结

​    Git分支：master/main；develop/dev；release/pre-release；feature/fea-；hotfix/fixbug等

​	Git分支模型、Gitee分支模型；

​	Git合并：快进式合并，默认；非快进式合并 --no-ff(fast-forward)。一般对公开仓库使用快进式合并。

​	提价修改或合并分支时，当同文同行的内容不同，将发生冲突——Git无法进行自动合并。

​	Git配置合并工具以及差异比较工具。

| 序号 | 名称     | 用法                     | 描述                         | 示例                                                         |
| ---- | -------- | ------------------------ | ---------------------------- | ------------------------------------------------------------ |
| 1    | clone    | git clone URL            | 克隆，远程库克隆本地库       | git clone URL https://gitee.com/osgisOne/cmpd_gitopsc.git    |
| 2    | status   | git status               | 库状态                       | git status                                                   |
| 3    | add      | git add .                | 添加当前目录所有文件         | git add .                                                    |
| 4    | commit   | git commit -m "***"      | 提交暂存区文件入库           | git commit -m "描述提交内容的信息"                           |
| 5    | push     | git push                 | 推送本地库至远程库           | git push                                                     |
| 6    | checkout | git checkout develop     | 签出分支                     | git checkout develop                                         |
| 7    | merge    | git merge hotfix         | 快进式合并hotfix到当前分支   | git merge hotfix，将hotfix直接合并到当前分支，当前分支线性化，掩盖历史 |
| 8    | merge    | git merge --no-ff hotfix | 非快进式合并hotfix到当前分支 | git merge hotfix，将hotfix创建新结点，再合并到当前分支，保留hotfix分支，当前非线性化，保留历史 |
| 9    | branch   | git branch -a -b name    | 分支                         | 显示、创建、切换分支                                         |

