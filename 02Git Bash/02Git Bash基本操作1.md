## Git Bash基本操作1

#### 1.Git由来

Linus大神仅用了两周时间，自已用C写了一个分布式版本控制系统。

![image-20220924193956700](assets/image-20220924193956700.png)

林纳斯·本纳第克特·托瓦兹（Linus Benedict Torvalds，1969年12月28日- ），芬兰赫尔辛基人，著名的电脑程序员，Linux内核的发明人及该计划的合作者 [1] ，毕业于赫尔辛基大学计算机系，1997年至2003年在美国加州硅谷任职于全美达公司（Transmeta Corporation），现受聘于开放源代码开发实验室（OSDL：Open Source Development Labs, Inc），全力开发Linux内核。与妻子托芙（Tove，芬兰前女子空手道冠军）育有三个女孩。著有自传《乐者为王》。

因为成功地开发了操作系统Linux内核，托瓦兹获得了2014年计算机先驱奖（For pioneering development of the Linux kernel using the open-source approach）。他的获奖创造了计算机先驱奖历史上的多个第一：第一次授予一位芬兰人；第一次授予一位“60后”（其实只差3天，就是“70后”）；获奖成果是在学生时期取得的。

![img](assets/fbe8d9e38985e171098b6915197b6ad5.png)

#### Git特点

**1.直接记录快照，而非差异比较**

**2. 近乎所有操作都是本地执行**

**3. 时刻保持数据完整性**

**4. 多数操作仅添加数据**

#### 2.Git优缺点

1. 适合分布式开发，强调个体。
2. 公共服务器压力和数据量都不会太大。
3. 速度快、灵活。
4. 任意两个开发者之间可以很容易的解决冲突。
5. 离线工作。

1. 模式上比SVN更加复杂。
2. 不符合常规思维。
3. 代码保密性差，一旦开发者把整个库克隆下来就可以完全公开所有代码和版本信息。

#### 3.Git原理

Git 三棵“树”组成。**工作目录树**，持有实际文件；缓存区（Index）**目录树**，临时保存改动；blob**对象树**，由HEAD指向最近一次提交后的结果。

![img](assets/trees.png)

#### 4.Git对象

**commit、tree、blob对象和tag对象**

![img](assets/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTIwNjAwMzM=,size_16,color_FFFFFF,t_70-166402268099320.png)

![b5876cad89b3c71a3ec7a0432609b7d1.png](assets/b5876cad89b3c71a3ec7a0432609b7d1.png)

#### 5.Git工作流

Git的工作总共分四层，其中三层是在自己本地也就是说git仓库（通常讲的仓库即指本地仓库），包括了工作目录、暂存区和本地仓库。

工作目录（workspace）就是执行命令git init时所在的地方，也就是执行一切文件操作的地方；暂存区（index）和本地仓库（repository）都是在.git目录下，因为它们只是用来存数据的。

远程仓库（remote）在中心服务器，也就是做好工作之后推送到远程仓库，或者从远程仓库更新下来最新代码到本地。

![img](assets/426b17635b0c685c051500af6a3c2b72.png)

**Git文件四种状态**

- Untracked：新增的文件的状态，未受Git管理，记录在工作区

- Modified：受Git管理过的文件的改动状态（包括改动内容、删除文件），记录在工作区

- Staged：将记录在工作区的文件变动状态通知了Git，记录在暂存区

- Unmodified：受Git管理中的文件状态（没有变动），记录在本地仓库/远程仓库

  ![img](assets/1944911-20201026161437540-1982967373.png)

#### 6.Git客户端Git Bash操作

Shell是文本解释器程序的统称，常见的Shell有sh、bash、ksh、rsh、csh等。sh的全名是Bourne Shell。名字中的玻恩就是这个Shell的作者。而bash的全名是Bourne Again Shell。

![image-20220924201030472](assets/image-20220924201030472.png)

#### 7.签出分支dev

方法一：先克隆全库，再签出分支：git clone url      git checkout develop

方法二：克隆单分支：git clone -b develop--single-branch https://gitee.com/osgisOne/cmpd_gitopsc.git

#### 8.本地开发JWSCalculator项目

![img](assets/790652e6d83be84e0f501ee0d5f4089d.png)

该案例已经讲解，将是练习、考试案例，希望很好的理解、掌握。

![image-20220924202235507](assets/image-20220924202235507.png)

#### 9.推送项目至远程库

git add .

git commit -m"*** "

git push

#### 10.作业截图

![img](assets/58def5b0c0b323fc827c50860a679ba1.png)

javac *.java sdk/*.java plugins/*.java

![img](assets/d94529f6aa0270473ff2590bcb5c1ffe.png)

java gitops.jwscalculator.JwsCalculator

cd 到目录：src\main\java\gitops\jwscalculator，或者大家使用JavaIDE进行操作。

![img](assets/d94529f6aa0270473ff2590bcb5c1ffe.png)

编译命令改为：

javac *.java sdk/*.java plugins/*.java -d .

原因是有包gitops.jwscalculator，编译时多一个-d参数和后面一个点号. 表示当前目录位置。最后的目录结构与视频中的一样，但是编译时要加参数，以包展开。

然后相同目录下：

java gitops.jwscalculator.JwsCalculator

![img](assets/9acc16cedc37115cd59c88042feea41d.png)

**发现Bug：视频中的编译执行操作都是对的，报错是因为插件源码文件夹与命名空间不一致导致的。源码是plugins，命名空间是plugin。加参数-d和指明路径.就可以了。编译时，将各插件class文件放到了plugin文件夹与其命名空间对应。**

![img](assets/acf29fa6e676056dbb9184b7092cf095.png)

#### 11.作业视频

![image-20220924201845661](assets/image-20220924201845661.png)

#### 小结

​    Git特点：分布式、多分支、开源开发

​    Git工作流：正向、方向、差异

​    Git四层结构：工作区、缓冲区、本地库、远程库

​    Git四个对象：Commit、tree、blob、tag

​	Git文件四种状态：未跟踪、未修改、已修改、已暂存

​    JwsCalculator项目7个版本

| 序号 | 名称     | 用法                 | 描述                   | 示例                                                      |
| ---- | -------- | -------------------- | ---------------------- | --------------------------------------------------------- |
| 1    | clone    | git clone URL        | 克隆，远程库克隆本地库 | git clone URL https://gitee.com/osgisOne/cmpd_gitopsc.git |
| 2    | status   | git status           | 库状态                 | git status                                                |
| 3    | add      | git add .            | 添加当前目录所有文件   | git add .                                                 |
| 4    | commit   | git commit -m "***"  | 提交暂存区文件入库     | git commit -m "描述提交内容的信息"                        |
| 5    | push     | git push             | 推送本地库至远程库     | git push                                                  |
| 6    | checkout | git checkout develop | 签出分支               | git checkout develop                                      |

