## Git Bash基本操作3

#### **1.Git 变基Rebase**

###### 变基原理

变基就是快速合并变成线性历史提交记录。

git rebase 变基到分支  #将当前分支变基到某分支

变基本质是将提取C5和C6中的补丁和修改，并且将其在C7上重新作用一次，然后生成一个新的提交。

![img](assets/b103f4fc0b0aebd405cf1f79f515f92a.png)

在testing分支进行变基操作到master后，git会进行如下操作：

- 找到testing分支和master分支的共同祖先，即C4。
- 收集共同祖先C4到目前提交C6的所有修改和补丁，放到临时文件中。
- 将目前的分支testing指向目标基底master。
- 在testing上依次应用临时文件中所有的修改和补丁。本例中为将C5和C6的修改和补丁依次应用。

- 生成一个新的提交，即C8。

三方合并和变基生成的新提交C8内容是一致的，不同的是提交历史：三方合并能够清楚地看到合并的历史，而变基只有一条串联的历史。

**如果你要向他人维护的项目贡献代码，就可以将自己的修改都变基到master上，推送之后，维护者只需要进行快进操作。**

###### **变基实践**

**master分支: Mul.java**

**![img](assets/d4365cb447657263e994f387ab58a61e.png)**

**Dev分支: Mul.java**

**![img](assets/d4365cb447657263e994f387ab58a61e-166625382505339.png)**

**变基冲突**

**也是同一文件，在同一行出现冲突**！

**![img](assets/f4bd355ccbdbe3cc61569f875a3c3f16.png)**

**vim Mul.java**

![img](assets/2dc0befb16ceab8690f6790f1cb46d3f.png)

**![img](assets/50a67c6f788c72104e8c5975b901d60c.png)**

![img](assets/1230bc2258ec03205f0eff03afd06876.png)

###### 变基过程

- 合并master与dev分支上的Mul.java文件出现冲突
- git checkout dev #签出dev分支
- git rebase master #变基到master分支（位于develop）

error: could not apply 094ad27... Merge vs Rebase

Resolve all conflicts manually, mark them as resolved with

"git add/rm <conflicted_files>", then run "git rebase --continue".

You can instead skip this commit: run "git rebase --skip".

To abort and get back to the state before "git rebase", run "git rebase --abort".

Could not apply 094ad27... Merge vs Rebase

Auto-merging dev/Mul.java

CONFLICT (content): Merge conflict in dev/Mul.java

- 根据git提示：手工处理冲突，并保存Mult.java文件
- git add Mul.java
- git rebase --continue
- 也可解决冲突后，采用：checkout主分支，快进合并分支到主分支
- git checkout master
- git merge dev

![img](assets/0f2421e8ceac492cde218a7c2ec93840.png)

![img](assets/b1fc69e5bcc6ecdead65b992d013a63f.png)

![img](assets/bc0ed084d1d5ca0e554cdb8d06437553.png)

视频中的计算器变基

![img](assets/61d7a422b359d0d8851bcf438d5a5d37.png)

###### 变基合并

**请注意，无论是通过变基，还是通过三方合并，整合的最终结果所指向的快照始终是一样的，只不过提交历史不同罢了。 变基是将一系列提交按照原有次序依次应用到另一分支上，而合并是把最终结果合在一起。**

git rebase 的**优势**是创造更线性的提交历史，使得代码库的提交历史变得异常清晰，**劣势**是缺失了分支信息,好像从没存在过该分支一样。

合并本质是将两个分支的最新快照以及共同祖先进行三方合并，并且生成一个新的快照。

![img](assets/fdee7b8468d5708a643d55a8af8b4dbe.png)

###### 何时变基

如果是公开且与其他人共享的分支，那么重写公开的共享分支，将会搞砸团队中的其他会员。

要是提交分支的精确历史重要（因为变基将重写提交历史，但没有人愿意阅读书的第一草稿）。

根据上述准则，我会针对短期生命的本地分支使用变基，而对 公开仓库的分支使用合并。

###### 变基的风险

变基应遵循一条准则：一旦分支中的提交的对象发布到公共仓库，就千万不要对该分支进行变基操作。

#### **2.Git 分支显示**

- `--pretty="..."` 定义输出的格式
- `%h` 是提交 hash 的缩写
- `%d` 是提交的装饰（如分支头或标签）
- `%ad` 是创作日期
- `%s` 是注释
- `%an` 是作者姓名
- `--graph` 使用 ASCII 图形布局显示提交树
- `--date=short` 保留日期格式更好且更短

**git log --oneline --decorate --graph --all**

![img](assets/9ced1a67f91abea81cc2a19123e0b8ec.png)

![img](assets/7782186d27b4f69f0cc0095db73ae29c.png)

**git log --graph --all --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative**

![img](assets/bcac81c50529cfec6d2ce6b2322b68ad.png)

![img](assets/60461b1894277f9b347897aac2bbb18a.png)

#### 3.鸿蒙操作系统文档分叉管理实例

https://gitee.com/czldl/docs.git

![img](assets/dd5ee0d268e5232877786586b87103b5.png)

![img](assets/2d5552427bc331fe70e6157d761042e5.png)

#### 小结

Git变基：git rebase master

Git分支显示：git log小结

Git分支：master/main；develop/dev；release/pre-release；feature/fea-；hotfix/fixbug等

Git分支模型、Gitee分支模型；

Git合并：快进式合并，默认；非快进式合并 --no-ff(fast-forward)。一般对公开仓库使用快进式合并。

提交修改或合并分支时，当同文同行的内容不同，将发生冲突——Git无法进行自动合并。

Git配置合并工具以及差异比较工具。

| 序号 | 名称     | 用法                     | 描述                         | 示例                                                         |
| ---- | -------- | ------------------------ | ---------------------------- | ------------------------------------------------------------ |
| 1    | clone    | git clone URL            | 克隆，远程库克隆本地库       | git clone URL https://gitee.com/osgisOne/cmpd_gitopsc.git    |
| 2    | status   | git status               | 库状态                       | git status                                                   |
| 3    | add      | git add .                | 添加当前目录所有文件         | git add .                                                    |
| 4    | commit   | git commit -m "***"      | 提交暂存区文件入库           | git commit -m "描述提交内容的信息"                           |
| 5    | push     | git push                 | 推送本地库至远程库           | git push                                                     |
| 6    | checkout | git checkout develop     | 签出分支                     | git checkout develop                                         |
| 7    | merge    | git merge hotfix         | 快进式合并hotfix到当前分支   | git merge hotfix，将hotfix直接合并到当前分支，当前分支线性化，掩盖历史 |
| 8    | merge    | git merge --no-ff hotfix | 非快进式合并hotfix到当前分支 | git merge hotfix，将hotfix创建新结点，再合并到当前分支，保留hotfix分支，当前非线性化，保留历史 |
| 9    | branch   | git branch -a -b name    | 分支                         | 显示、创建、切换分支                                         |
| 10   | rebase   | git rebase master        | 变基                         | 将当前分支变基到master分支                                   |
| 11   | log      | git log                  | 日志                         | git log --oneline --decorate --graph --all                               git log --graph --all --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative |
